import React from 'react';
import Icon, { parseIconFromClassName, } from './Icon';
export default class AppIcon extends React.PureComponent {
    render() {
        const props = this.props;
        let defaultStyles = {
            fontSize: props.size ? props.size : 12
        };
        let styles = {
            ...defaultStyles,
            ...props.style
        };
        if (props.type === "fad") {
            let stylesBefore = {
                ...styles,
                opacity: 0.4,
            };
            let stylesAfter = {
                ...styles,
                marginTop: (props.size * -1),
            };
            const parsedIconBefore = parseIconFromClassName(props.icon);
            const parsedIcon = parseIconFromClassName(props.icon, true);
            return (React.createElement(React.Fragment, null,
                React.createElement(Icon, { style: stylesBefore, type: "fas", icon: parsedIconBefore }),
                React.createElement(Icon, { style: stylesAfter, type: props.type, icon: parsedIcon })));
        }
        const parsedIcon = parseIconFromClassName(props.icon);
        return (React.createElement(Icon, { style: styles, type: props.type, icon: parsedIcon }));
    }
}
