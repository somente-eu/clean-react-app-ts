import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { View, Text } from 'native-base';
import { StyleSheet, PixelRatio } from 'react-native';
import Global from '../helpers/global';
import AppIcon from '../components/AppIcon';
class TabIcon extends Component {
    render() {
        var color = this.props.selected ? '#00f240' : '#55f';
        return (React.createElement(View, { style: { flex: 1, flexDirection: 'column', alignItems: 'center', alignSelf: 'center', justifyContent: 'center' } },
            React.createElement(AppIcon, { size: 24, type: "fad", icon: this.props.iconName, style: { color } })));
    }
}
export default class ScreenRouter extends Component {
    constructor(props) {
        super(props);
        Global.ScreenRouter = this;
    }
    render() {
        const { userData } = Global.AppProps;
        console.log(userData);
        return (React.createElement(Router, null,
            React.createElement(Scene, { key: "root" },
                React.createElement(Scene, { key: "main", tabs: true, tabBarStyle: styles.tabBar, default: "tab1", hideNavBar: true },
                    React.createElement(Scene, { key: "menu", title: "Menu", iconName: "fa-bars", icon: TabIcon, hideNavBar: true, component: () => { return React.createElement(Text, null, "Teste Component"); } })))));
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    tabBar: {
        borderTopColor: 'darkgrey',
        borderTopWidth: 1 / PixelRatio.get(),
        backgroundColor: 'ghostwhite',
        opacity: 0.98
    },
    navigationBarStyle: {
        backgroundColor: 'red',
    },
    navigationBarTitleStyle: {
        color: 'white',
    },
});
