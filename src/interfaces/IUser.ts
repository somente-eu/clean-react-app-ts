export default interface IUser {
    id: number;
    name: string;
    username: string;
    email: string;
    token: string;
    approv: boolean;
}