import IUser from "./IUser";

export default interface IAppProps {
    userData?: IUser;
}