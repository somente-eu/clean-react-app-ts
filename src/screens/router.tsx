import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { View, Text } from 'native-base';
import { StyleSheet, PixelRatio } from 'react-native';
import Global from '../helpers/global';
import AppIcon from '../components/AppIcon';


//Create a dedicated class that will manage the tabBar icon
interface ITabIconProps {
    selected: boolean;
    iconName: string;
    title: string;
}

class TabIcon extends Component<ITabIconProps> {
    render() {
        var color = this.props.selected ? '#00f240' : '#55f';

        return (
            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', alignSelf: 'center', justifyContent: 'center' }}>
                <AppIcon size={24} type="fad" icon={this.props.iconName} style={{ color }} />
            </View>
        );
    }
}

export default class ScreenRouter extends Component {
    constructor(props: any) {
        super(props)
        Global.ScreenRouter = this
    }

    render() {
        const { userData } = Global.AppProps
        console.log(userData)

        return (
            <Router>
                <Scene key="root">
                    <Scene key="main" tabs={true} tabBarStyle={styles.tabBar} default="tab1" hideNavBar={true}>
                        <Scene
                            key="menu"
                            title="Menu"
                            iconName="fa-bars"
                            icon={TabIcon}
                            hideNavBar={true}
                            component={() => { return <Text>Teste Component</Text> }}
                        />

                    </Scene>
                </Scene>
            </Router>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    tabBar: {
        borderTopColor: 'darkgrey',
        borderTopWidth: 1 / PixelRatio.get(),
        backgroundColor: 'ghostwhite',
        opacity: 0.98
    },
    navigationBarStyle: {
        backgroundColor: 'red',
    },
    navigationBarTitleStyle: {
        color: 'white',
    },
});