import React from 'react';
import Icon, {
    parseIconFromClassName,
} from './Icon';


interface IProps {
    type: "fal" | "far" | "fas" | "fab" | "fad";
    style?: any;
    icon: string;
    size: number;
}

export default class AppIcon extends React.PureComponent<IProps> {
    render() {
        const props = this.props;
        let defaultStyles = {
            fontSize: props.size ? props.size : 12
        };

        let styles = {
            ...defaultStyles,
            ...props.style
        };

        if (props.type === "fad") {
            let stylesBefore = {
                ...styles,
                opacity: 0.4,
            };
            let stylesAfter = {
                ...styles,
                marginTop: (props.size * -1),
            };

            const parsedIconBefore: any = parseIconFromClassName(props.icon);
            const parsedIcon: any = parseIconFromClassName(props.icon, true);

            return (
                <>
                    <Icon style={stylesBefore} type={"fas"} icon={parsedIconBefore} />
                    <Icon style={stylesAfter} type={props.type} icon={parsedIcon} />
                </>
            );
        }

        const parsedIcon: any = parseIconFromClassName(props.icon);
        return (<Icon style={styles} type={props.type} icon={parsedIcon} />);
    }
}