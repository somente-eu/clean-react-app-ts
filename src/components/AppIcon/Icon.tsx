import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';

import Icons, { DualToneIcons } from './FontAwesomeIcons';
const IconTypes = {
  fal: 'FontAwesome5Pro-Light',
  far: 'FontAwesome5Pro-Regular',
  fas: 'FontAwesome5Pro-Solid',
  fab: 'FontAwesome5Brands-Regular',
  fad: 'FontAwesome5Duotone-Solid',
}

const parseIconFromClassName = (iconName: string, dualTone?: boolean) => {
  if (!iconName) return;

  iconName = iconName.replace(/(fa\-)/gi, '')
  iconName = iconName.replace(/(fa|fas|far|fal)( )/gi, '')

  let nameParts = iconName.match(/(\-)(\w{1,1})/gi) || []

  nameParts.forEach(m => {
    iconName = iconName.replace(m, m.toUpperCase())
  })

  iconName = iconName.replace(/\-/gi, '')
  iconName = (iconName || '').trim()

  return dualTone ? DualToneIcons[iconName] : Icons[iconName]
}

interface IProps {
  type: "fal" | "far" | "fas" | "fab" | "fad";
  style?: any;
  color?: string;
  children?: any;
  icon: string;
}

class Icon extends Component<IProps, any> {
  _root: any;

  render() {
    const { style, color, type, icon } = this.props;
    const font = { fontFamily: IconTypes[type] }
    return (
      <Text
        style={[styles.icon, { color }, style, font]}
        ref={component => this._root = component}
      >
        {icon}
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    backgroundColor: 'transparent'
  },
});

export { Icons, IconTypes, parseIconFromClassName };
export default Icon;
