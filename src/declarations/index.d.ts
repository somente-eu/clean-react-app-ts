declare module 'react-native-vector-icons'

declare interface JSON {
    decycle: any
    retrocycle: any
}

declare namespace React {
    interface Component {
        setLoadState: (element: string) => void
        unsetLoadState: (element: string) => void
        onLoadState: (element: string) => boolean
    }
}

declare interface String {
    replaceAt: (index: number, replace: string) => string
}

declare interface Number {
    toMoney: () => string
}