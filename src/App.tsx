/**
 * @format
 * @flow strict-local
*/

import * as React from "react";
import { Root } from "native-base";
import Global from './helpers/global';
import ScreenRouter from "./screens/router";
import { YellowBox } from 'react-native';

export default class App extends React.Component {
  componentWillMount() {
    Global.overridePrototypes();

    YellowBox.ignoreWarnings(['Remote debugger is in a background tab'])
  }

  render() {
    return (
      <Root>
        <ScreenRouter />
      </Root>
    );
  }
}