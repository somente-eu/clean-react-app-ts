import React from 'react';
import IAppProps from '../interfaces/IAppProps';
import ScreenRouter from '../screens/router';

export default class Global {
    static AppProps: IAppProps = {};
    static ScreenRouter: ScreenRouter = new ScreenRouter({})

    static overridePrototypes() {

        // React
        // Component
        React.Component.prototype.setLoadState = function (element: string) {
            if (!this.state)
                this.state = {};
            if (!this.state.loading) {
                let arr: Array<string> | any = [];
                arr[element] = true;
                this.setState({ loading: arr });
            } else {
                let loading = this.state.loading;
                loading[element] = true;
                this.setState({ loading });
            }
        };

        React.Component.prototype.unsetLoadState = function (element: string) {
            if (!this.state)
                this.state = {};
            if (!this.state.loading) {
                let arr: string[] | any = [];
                arr[element] = false;
                this.setState({ loading: arr });
            } else {
                let loading = this.state.loading;
                loading[element] = false;
                this.setState({ loading });
            }
        };

        React.Component.prototype.onLoadState = function (element: string) {
            if (!this.state)
                this.state = {};
            if (!this.state.loading)
                return false;
            return this.state.loading[element] === true;
        };

        // JavaScript
        // Text
        String.prototype.replaceAt = function (index: number, replacement: string) {
            return this.substr(0, index) + replacement + this.substr(index + replacement.length);
        }

        // Number
        Number.prototype.toMoney = function () {
            try {
                let num = this.toFixed(2).replace(`.`, `,`).toString();
                let snum = num.substr(0, num.length - 3);
                let lnum = num.length % 3;
                if (lnum > 2) {
                    let lnum1 = snum.substr(0, lnum);
                    let lnum2: any = snum.substr(lnum, snum.length - lnum);
                    let pnum = lnum2.match(/.{3}/g).join('.');
                    let rnum = (lnum1 + '.' + pnum);
                    if (lnum === 0)
                        rnum = pnum;
                    let fnum = rnum + num.substr(num.length - 3, 3);
                    //console.log({ num, snum, lnum, lnum1, lnum2, pnum, rnum, fnum });
                    return `R$ ${fnum}`;
                } else {
                    return `R$ ${num}`;
                }
            } catch (err) {
                //console.log(err)
                return "";
            }
        };
    }
}